package com.chain.rabbitmq;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * @author lian.ran
 * @Title: MqManager
 * @ProjectName demo
 * @Description: TODO
 * @date 2018/9/199:50
 */
public class MqManager {

    public static Connection newConnection()throws Exception{
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("127.0.0.1");
        factory.setUsername("guest");
        factory.setPassword("guest");
        factory.setVirtualHost("/");
        // 获取到连接以及mq通道
        Connection connection = factory.newConnection();
        return connection;
    }
}
