package com.chain.rabbitmq;

import java.io.IOException;
import java.util.Date;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;  
import com.rabbitmq.client.Channel;  
import com.rabbitmq.client.Connection;  
import com.rabbitmq.client.Consumer;  
import com.rabbitmq.client.DefaultConsumer;  
import com.rabbitmq.client.Envelope;  
  
/** 
 * 延时消息处理类 
 * @author
 * <p>
 *     即使一个消息比在同一队列中的其他消息提前过期，提前过期的也不会优先进入死信队列，它们还是按照入库的顺序让消费者消费。
 *     如果第一进去的消息过期时间是1小时，那么死信队列的消费者也许等1小时才能收到第一个消息。
 *     参考官方文档发现“Only when expired messages reach the head of a queue will they actually be discarded (or dead-lettered).”只有当过期的消息到了队列的顶端（队首），
 *     才会被真正的丢弃或者进入死信队列。所以在考虑使用RabbitMQ来实现延迟任务队列的时候，需要确保业务上每个任务的延迟时间是一致的。
 *    如果遇到不同的任务类型需要不同的延时的话，需要为每一种不同延迟时间的消息建立单独的消息队列
 *
 *
 * </p>
 */

public class DelayRecv {
    private static final String EXCHANGE_NAME = "timeout-exchange";
    private final static String QUEUE_NAME = "my.timeout.message2";
    private static final String ROUTKEY="test.timeout.message";
    /** 
     * 创建队列并声明consumer用于处理转发过来的延时消息 
     * @throws Exception 
     */  
    public static void delayRecv() throws Exception{  
        Connection conn=MqManager.newConnection();  
        final  Channel channel=conn.createChannel();
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT,true);
        String queueName=channel.queueDeclare().getQueue();
        System.out.println("队列名称"+queueName);
        channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, ROUTKEY);
        Consumer consumer=new DefaultConsumer(channel){  
            @Override  
            public void handleDelivery(String consumerTag,Envelope envelope,AMQP.BasicProperties properties,byte[] body) throws IOException{
                String mes= new String(body);
                System.out.println(envelope.getRoutingKey()+":delay Received :'"+mes+"' done,接受时间:"+DateUtils.dataToStr(new Date()));
               // channel.basicAck(envelope.getDeliveryTag(), true);
//                if(mes.indexOf("8")>0){
//                    System.out.println("测试回滚");
//                    channel.basicNack(envelope.getDeliveryTag(),true, true);
//                    System.out.println("测试回滚完成");
//                }
            }  
        };
        System.out.println("执行开始 ");
        //关闭自动应答机制，默认开启；这时候需要手动进行应该  
      channel.basicConsume(QUEUE_NAME, true, consumer);

        System.out.println("执行完了");
    }  
      
    public static void main(String[] args) throws Exception {  
        delayRecv();  
    }  
  
} 