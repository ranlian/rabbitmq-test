package com.chain.rabbitmq;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.rabbitmq.client.AMQP.BasicProperties;  
import com.rabbitmq.client.AMQP.BasicProperties.Builder;  
import com.rabbitmq.client.BuiltinExchangeType;  
import com.rabbitmq.client.Channel;  
import com.rabbitmq.client.Connection;  
  
/**
 * https://www.cnblogs.com/haoxinyue/p/6613706.html
 * 发送消息类 
 * @author
 * 
 */  
public class Send{
    private static final String EXCHANGE_NAME = "test.message";

    private final static String QUEUE_NAME = "my.timeout.message";

    private static final String ROUTKEY="my.routing.key";
    /** 
     * 在topic转发器的基础上练习延时转发，发送消息时指定消息过期时间 
     * 消息已发送到queue上，但未有consumer进行消费 
     * @param object 消息主体 
     * @throws IOException 
     */  
    public static void sendAToB(String object) throws Exception{
        Connection conn=MqManager.newConnection();  
        Channel channel=conn.createChannel();  
        //声明headers转发器  
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.HEADERS,true);
        //定义headers存储的键值对  
        Map<String, Object> headers=new HashMap<String, Object>();  
        headers.put("key", "123456");  
        headers.put("token", "654321");
      Map<String, Object> args = new HashMap<String, Object>();
       // args.put("x-message-ttl",12000); //消息过去
        args.put("x-dead-letter-exchange", "timeout-exchange");//过期消息转向路由
        args.put("x-dead-letter-routing-key", "test.timeout.message");//过期消息转向路由相匹配routingkey
       channel.queueDeclare(QUEUE_NAME, true, false, false, args);
        //把键值对放在properties  
        Builder properties=new BasicProperties.Builder();  
        properties.headers(headers);  
        properties.deliveryMode(2);//持久化
        //指定消息过期时间为12秒,队列上也可以指定消息的过期时间，两者以较小时间为准  
        properties.expiration("12000");//(12000)延时12秒，不会及时删除(在consuemr消费时判定是否过期，因为每条消息的过期时间不一致，删除过期消息就需要扫描整个队列)
        channel.basicPublish(EXCHANGE_NAME,ROUTKEY ,properties.build(),object.getBytes());
        System.out.println("Send '"+object+"'"+",时间:"+DateUtils.dataToStr(new Date()));
        channel.close();  
        conn.close();  
    }
    public static void main(String[] args) throws Exception {  
        sendAToB("我开始测试延时消息了3!");
    }  
} 