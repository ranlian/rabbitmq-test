package com.chain.rabbitmq;


import com.rabbitmq.client.*;

import java.util.HashMap;
import java.util.Map;

public class SendTest {
    private static final String EXCHANGE_NAME = "exchange.ttl.test";
    private final static String QUEUE_NAME = "queue.ttl.test";
    private static final String ROUTKEY="ttl";
    public static void main(String[] argv) throws Exception {
        // 获取到连接以及mq通道
        // 获取到连接以及mq通道
        Connection connection =MqManager.newConnection();
        Channel channel = connection.createChannel();
        // 声明exchange
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT,true);

        /**
         * 设置死信队列
         */
        Map<String, Object> args = new HashMap<String, Object>();
        //args.put("vhost", "dev");
        // args.put("username","dev");
        //args.put("password", "dev");
         args.put("x-message-ttl",60000);
       // args.put("x-expires", 6000);
        args.put("x-dead-letter-exchange", "test.exchange.name");
        args.put("x-dead-letter-routing-key",QUEUE_NAME);

         channel.queueDeclare(QUEUE_NAME, true, false, false, args);
        // 消息内容
        String message = "Hello World oouuu";

        AMQP.BasicProperties properties = new AMQP.BasicProperties();
        properties.builder().expiration("60000").build();
        //  properties.setExpiration("60000");//设置消息的过期时间为60秒

      channel.basicPublish(EXCHANGE_NAME, ROUTKEY, null, message.getBytes());
      //  channel.basicPublish(EXCHANGE_NAME, ROUTKEY, properties, message.getBytes());
//        byte i = 10;
//        while (i-- > 0) {
//            channel.basicPublish(EXCHANGE_NAME, ROUTKEY, new AMQP.BasicProperties.Builder().expiration(String.valueOf(i * 1000)).build(),
//                    new byte[]{i});
//        }
        System.out.println(" [x] Sent '" + message + "'");
        channel.close();
        connection.close();
    }  
} 