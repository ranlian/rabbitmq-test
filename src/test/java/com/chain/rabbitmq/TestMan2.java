package com.chain.rabbitmq;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by lian.ran on 2017/11/16.
 */
public class TestMan2 {
    private static final String EXCHANGE_NAME = "exchange.ttl.test";
    private final static String QUEUE_NAME = "queue.ttl.test";
    private static final String ROUTKEY="ttl";
    public static void main(String[] args) throws Exception {
        Connection conn =   MqManager.newConnection();
        Channel channel = conn.createChannel();
        CusumerThread thread = new CusumerThread(1+"TestMan2",QUEUE_NAME,ROUTKEY,conn,channel);
        Thread t = new Thread(thread);
        t.start();
    }


}
