package com.chain.rabbitmq;

import sun.nio.ch.SelectorImpl;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author lian.ran
 * @Title: DateUtils
 * @ProjectName demo
 * @Description: TODO
 * @date 2018/9/1917:44
 */
public class DateUtils {


    public static String dataToStr(Date date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
       return format.format(date);
    }
}
