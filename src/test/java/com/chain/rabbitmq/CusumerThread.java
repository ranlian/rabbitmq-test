package com.chain.rabbitmq;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

/**
 * @author
 *
 */
public class CusumerThread implements Runnable {

	private static final String EXCHANGE_NAME = "test.exchange.name";


	private String queueName;

	private String name;

	private String rKey;
	private  Connection conn;

	Channel channel;

	public CusumerThread(String name, String queueName, String rKey, Connection conn, Channel channel ) {
		super();
        this.queueName = queueName;
		this.name = name;
		this.rKey = rKey;
		this.conn = conn;
		this.channel = channel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {

//			int activeCount = TestMan.exeService.getActiveCount();
//			long taskCount = TestMan. exeService.getTaskCount();
//			long poolSize = TestMan.exeService.getPoolSize();
//			System.out.println("消费者任务:"+ System.currentTimeMillis()+","+"线程:[main],activeCount:"+activeCount+",taskCount:"+taskCount+",poolSize:"+poolSize);
			// 创建交换机
			channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT, true);
			Map<String, Object> args = new HashMap<String, Object>();
			//args.put("vhost", "dev");
			// args.put("username","dev");
			//args.put("password", "dev");
			args.put("x-message-ttl",60000);
			args.put("x-dead-letter-exchange", "test.exchange.name");
			args.put("x-dead-letter-routing-key",queueName);
			// 创建队列
			channel.queueDeclare(queueName, true, false, false, args);
			//channel.basicQos(0,1,false);
			//channel.basicQos(1);
			// 绑定队列
			channel.queueBind(queueName, EXCHANGE_NAME, rKey);
//			channel.addShutdownListener(new ShutdownListener() {
//				@Override
//				public void shutdownCompleted(ShutdownSignalException cause) {
//					System.out.println("====guanb关闭啦");
//				}
//			});
			/*
         while (true){
			 GetResponse response = channel.basicGet(queueName,false);
			 if(response!=null){
				 String mes = new String(response.getBody(),"UTF-8");
				 if(mes!=null){
					 System.out.println("消费信息:"+new String(response.getBody(),"UTF-8"));
				 }
				 long deliveryTag = response.getEnvelope().getDeliveryTag();
				 System.out.println("正常消费,deliveryTag:"+deliveryTag);
				 channel.basicAck(deliveryTag,false);
			 }
			 System.out.println("休息1秒");
			 Thread.sleep(1000);

		 }*/

			channel.basicConsume(queueName, false, new DefaultConsumer(channel) {
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
										   byte[] body) throws IOException {
					int actionType = 0;
					String message = "";
					try {
						super.handleDelivery(consumerTag, envelope, properties, body);
						//String routingKey = envelope.getRoutingKey();
						//LOGGER.info("消费者的routingKey{}",rKey);
						message = new String(body,"UTF-8");
						System.out.println("从rabbitMq消费消息,message"+message);
					} catch (Exception e) {
						actionType = 1;
						System.out.println("从rabbitMq消费消息失败,message"+message);
					}finally {
						switch (actionType){
							case 0: //执行完毕后提交确认请求
								channel.basicAck(envelope.getDeliveryTag(), true);
								break;
							case 1:
								channel.basicNack(envelope.getDeliveryTag(),true,true);
								break;
						}
					}
				}
			});


		} catch (IOException e) {
			e.printStackTrace();
		}  catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void close(){
		if(channel!=null){
			try {
				channel.close();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (TimeoutException e) {
				e.printStackTrace();
			}
		}
	}

}